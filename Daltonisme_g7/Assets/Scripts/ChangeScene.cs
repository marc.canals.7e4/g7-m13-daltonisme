﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour
{
    public int level;

    public void LoadA(string scenename)
    {
        SceneManager.LoadScene(scenename);

    }

    public void LoadALevel1(string scenename)
    {
        level = 1;
        SceneManager.LoadScene(scenename);
        
    }

    public void LoadALevel2(string scenename)
    {
        level = 2;
        SceneManager.LoadScene(scenename);
        
    }

    public void LoadALevel3(string scenename)
    {
        level = 3;
        SceneManager.LoadScene(scenename);
       
    }

    public void LoadALevel4(string scenename)
    {
        level = 4;
        SceneManager.LoadScene(scenename);
        
    }
    private void OnDisable()
    {
        PlayerPrefs.SetInt("level", level);
    }

    private IEnumerator Start()
    {
        if (SceneManager.GetActiveScene().name.Equals("SplashScreen"))
        {
            yield return new WaitForSeconds(3);
            SceneManager.LoadScene("MenuScreen");
        }
    }




}
