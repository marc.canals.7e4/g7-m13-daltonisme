﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Array2DController : MonoBehaviour
{
    public int level, vides;
    public float stage;
    public int[,] mapaMatriu = new int[10, 10];
    public GameObject[,] matriuSprites = new GameObject[10, 10];
    private GameObject prefab, grid, clon;
    private int squareCR, squareCC, contador, lenght;
    private Button levelSelected;
    private Renderer[,] renderer = new Renderer[10,10];
    private Slider ProgressBar;
    private Text progress;

    private void Awake()
    {
        grid = GameObject.Find("Grid");
        ProgressBar = GameObject.Find("ProgressBar").gameObject.GetComponent<Slider>();
        level = PlayerPrefs.GetInt("level");
        progress = GameObject.Find("Percentatge").GetComponent<Text>();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        vides = 2;
        lenght = mapaMatriu.Length / 10;
        progress.text = "Progress:";
        stage = 0.0f;
        prefab = GameObject.Find("spriteMatriu");
        initValuesMap();
        asignSqareCenter();
        createSquareInMap();
        initMatriu();

    }

    // Update is called once per frame
    void Update()
    {
        changeColor();
        waiter();
        changeLevel();
        
        
    }
    //Funcions
    public void changeLevel()
    {
        if (vides != 0)
        {
            if (level != 1)
            {
                if (stage < 20)
                {
                    ProgressBar.value = stage / 20;
                    progress.text = "Progress: " + stage * 5 + " %";
                }
                else
                {
                    SceneManager.LoadScene("StageCompleteScene");
                }
            }
            else if (stage == 1)
            {
                progress.text = "Progress: " + stage * 100 + " %";
                SceneManager.LoadScene("StageCompleteScene");
            }
        }
        else
        {
            SceneManager.LoadScene("FinalResultScreen");
        }
    }
    private IEnumerator waiter()
    {
        yield return new WaitForSeconds(1);
    }
    //Crea una matriu plena de 0's -> mapeado
    public void initValuesMap()
    {
        for (int row = 0; row < lenght; row++)
        {
            for (int col = 0; col < lenght; col++)
            {
                mapaMatriu[row, col] = 0;
            }
        }
    }
    //Asigna un punt central aleatori al mapejat de la matriu
    public void asignSqareCenter()
    {
        squareCR = Random.Range(1, lenght - 1);
        squareCC = Random.Range(1, lenght - 1);

        mapaMatriu[squareCR, squareCC] = 1;
    }
    //Mitjançant el punt central asignem el cuadrat de 3x3 de 1's
    public void createSquareInMap()
    {
        mapaMatriu[squareCR - 1, squareCC - 1] = 1;
        mapaMatriu[squareCR - 1, squareCC] = 1;
        mapaMatriu[squareCR - 1, squareCC + 1] = 1;
        mapaMatriu[squareCR, squareCC - 1] = 1;
        mapaMatriu[squareCR, squareCC + 1] = 1;
        mapaMatriu[squareCR + 1, squareCC - 1] = 1;
        mapaMatriu[squareCR + 1, squareCC] = 1;
        mapaMatriu[squareCR + 1, squareCC + 1] = 1;
    } 
    //Funcio que crea totes les copies del prefab i les guarda en un Array2D de renderers
    public void initMatriu()
    {
        for (int row = 0; row < lenght; row++)
        {
            for (int col = 0; col < lenght; col++)
            {
                clon = Instantiate(prefab, new Vector3(-2.7f + (row - row * 0.4f), 3 - (col - col * 0.4f), 0), Quaternion.identity, grid.transform);
                matriuSprites[row, col] = clon;
                renderer[row,col] = clon.gameObject.GetComponent<Renderer>();

            }
        }
    }
    //Funció que controla el color del quadrat i la resta
    void changeColor()
    {
        for (int row = 0; row < lenght; row++)
        {
            for (int col = 0; col < lenght; col++)
            {
                if (mapaMatriu[row, col] != 1)
                {
                    changeHSVceros(row,col);
                    matriuSprites[row, col].GetComponent<OnClicked>().IsCorrect = false;
                }
                else
                {
                    matriuSprites[row, col].GetComponent<OnClicked>().IsCorrect = true;
                    changeHSVcorrect(stage,row,col);
                }
            }
        }
    }
    //Funció que controla el color dels 4 nivells en el cas que no sigui el quadrat
    void changeHSVceros(int row, int col)
    {
        
            if (level == 1)
            {
                renderer[row,col].material.color = Color.HSVToRGB(1, 0, Random.Range(0.3f, 0.65f));
            }
            else if (level == 2)
            {
                renderer[row, col].material.color = Color.HSVToRGB(0.83f, 1, Random.Range(0.5f, 0.7f));
            }
            else if (level == 3)
            {
                renderer[row, col].material.color = Color.HSVToRGB(0.37f, 1, Random.Range(0.5f, 0.7f));
            }
            else
            {
                renderer[row, col].material.color = Color.HSVToRGB(0.6f, 1, Random.Range(0.5f, 0.7f));
            }
            contador++;


    }
    //Funció que controla el color dels 4 nivells en el cas que no sigui el quadrat
    void changeHSVcorrect(float stage, int row, int col)
        {
            
            if (level == 1)
            {
                renderer[row, col].material.color = Color.HSVToRGB(1, 0, Random.Range(0.8f, 1f));
            }
            else if (level == 2)
            {
                renderer[row, col].material.color = Color.HSVToRGB(0.83f, 1, Random.Range(0.8f - (stage / 100), 1f - (stage / 100)));
            }
            else if (level == 3)
            {
                renderer[row, col].material.color = Color.HSVToRGB(0.37f, 1, Random.Range(0.8f - (stage / 100), 1f - (stage / 100)));
            }
            else
            {
                renderer[row, col].material.color = Color.HSVToRGB(0.6f, 1, Random.Range(0.8f - (stage / 100), 1f - (stage / 100)));
            }
            contador++;

    }
    
    //Buttons stuff
        void reestartButton()
        {
            stage = 0.0f;
        }

        void iCantSeeBtton()
        {
            SceneManager.LoadScene("FinalResultScreen");
        }

    private void OnDisable()
    {
        PlayerPrefs.SetFloat("stage", stage);
    }
}


