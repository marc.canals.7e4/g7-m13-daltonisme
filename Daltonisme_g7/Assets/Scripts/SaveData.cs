﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveData : MonoBehaviour
{
    public int level;
    // Start is called before the first frame update
    void Awake()
    {
       
        level = PlayerPrefs.GetInt("level");
    }

    private void Update()
    {
        Debug.Log(level);
    }

    public void LoadNextLevel()
    {
        if (level != 4)
        {
            level += 1;
            SceneManager.LoadScene("LevelScreen");
        }
        else
        {
            SceneManager.LoadScene("FinalResultScreen");
        }
        
    }
    private void OnDisable()
    {

        PlayerPrefs.SetInt("level", level);
        PlayerPrefs.Save();
    }
}
