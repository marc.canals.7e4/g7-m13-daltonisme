﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClicked : MonoBehaviour
{
    public bool IsCorrect;
    private float stage;
    private int vides;
    private Array2DController controller;
    // Start is called before the first frame update
    void Start()
    {
        controller = GameObject.Find("MatriuManager").gameObject.GetComponent<Array2DController>();
        IsCorrect = false;
        stage = controller.stage;
        vides = controller.vides;
    }
    //Func when player hits the Collider of the Array2D
    private void OnMouseDown()
    {
        if (IsCorrect)
        {
            controller.initValuesMap();
            controller.asignSqareCenter();
            controller.createSquareInMap();
            controller.stage += 1;
        }
        else
        {
            vides--;
            controller.vides = vides;
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        stage = controller.stage;
        vides = controller.vides;

    }
}
