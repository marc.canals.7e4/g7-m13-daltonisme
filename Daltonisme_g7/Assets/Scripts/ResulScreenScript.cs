﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResulScreenScript : MonoBehaviour
{
    private int level;
    public Text Text;
    private float stage;
    // Start is called before the first frame update

    private void Awake()
    {
        stage = PlayerPrefs.GetFloat("stage");
    }
    void Start()
    {
        level = PlayerPrefs.GetInt("level");

        switch (level)
        {
            case 1: Text.text = "Acromatòpsia: \n"+stage*100 + "% completed";
                break;
            case 2: Text.text = "Protanopia:  \n" + stage * 5 + "% completed";
                break;
            case 3: Text.text = "Deuteranopia:  \n" + stage * 5 + "% completed";
                break;
            case 4:
                if (stage == 20)
                {
                    Text.text = "Enhorabona \n has completat \n el test \n correctament!";
                }
                else
                {
                    Text.text = "Tritanopia:  \n" + stage * 5 + "% completed";
                }
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
